<?php
$lang['ABNORMALBID'] = 'Maaf, iklan non-unggulan yang diposting membutuhkan penawaran abnormal.';
$lang['ABOUT_ME'] = 'Tentang Saya';
$lang['ABOUT_YOU'] = 'Tentang Anda';
$lang['ACCAEXIST'] = 'Maaf, akun dengan alamat email tersebut telah ada.';
$lang['ACCOUNT_SETTING'] = 'Pengaturan Akun';
$lang['ACCOUNTBAN'] = 'Akun ini telah diblokir.';
$lang['ACCOUNTCONFIRM'] = 'Akun ini harus dikonfirmasi sebelum Anda dapat masuk. Silakan periksa akun email Anda.';
$lang['AD_ID'] = 'Ad ID';
$lang['AD_LOCATION'] = 'Lokasi iklan';
$lang['AD_TITLE'] = 'Judul iklan Anda';
$lang['AD_VIEWS'] = 'Jumlah Tampilan Iklan';
$lang['ADD_FAVOURITE'] = 'Tambahkan Favorit';
$lang['ADD_MULTI_PHOTO'] = 'Anda dapat menambahkan banyak foto sekaligus.';
$lang['ADD_NICE_PHOTO'] = 'Tambahkan foto yang bagus ke iklan Anda.';
$lang['ADD_UP_TO'] = 'Tambahkan hingga 5 foto.';
$lang['ADDITIONAL_DETAILS'] = 'Detail Tambahan';
$lang['ADDITIONAL_INFO'] = 'Informasi Tambahan';
$lang['ADDRESS'] = 'Alamat';
$lang['ADDREVIEWS'] = 'Tambahkan ulasan Anda';
$lang['ADD_FILES'] = 'Tambahkan file';
$lang['ADD_FILES_TEXT'] = 'Tambahkan file ke antrian unggahan dan klik tombol mulai.';
$lang['ADNOTICE'] = 'Pemberitahuan Iklan';
$lang['ADS_DETAILS'] = 'Detail Iklan';
$lang['ADS_IMAGE'] = 'Gambar Iklan';
$lang['ADS_LISTINGS'] = 'Daftar Iklan';
$lang['ADSUCCESS'] = 'Iklan Anda berhasil diunggah. Mohon tunggu untuk disetujui. Terima kasih.';
$lang['ADTITLE_REQ'] = 'Judul Iklan diperlukan.';
$lang['ADVANCE_SEARCH'] = 'Pencarian Lanjutan';
$lang['ADVERTISE_WITH_US'] = 'Beriklan dengan Kami';
$lang['ADVWEBSITE'] = 'Mengiklankan situs web lain';
$lang['AD_DELETED'] = 'Iklan Telah Dihapus';
$lang['AD_DESCRIPTION'] = 'Ceritakan lebih banyak tentang iklan Anda';
$lang['AD_EXP_IN'] = 'Iklan kedaluwarsa dalam';
$lang['AD_POST'] = 'Posting iklan';
$lang['AD_UPLOADED_SUCCESS'] = 'Iklan Berhasil Diupload!';
$lang['AGO'] = 'lalu';
$lang['AGREE_COPYRIGHT'] = 'Anda harus setuju dengan hak cipta.';
$lang['ALL'] = 'Semua';
$lang['ALL_ADS'] = 'Semua Iklan';
$lang['ALL_CATEGORIES'] = 'Semua Kategori';
$lang['ALL_CATEGORY'] = 'Semua Kategori';
$lang['ALL_PACKAGES'] = 'Semua Paket';
$lang['ALL_RIGHT_RESERVED'] = 'Hak cipta dilindungi Undang-undang.';
$lang['ALREADY_EXIST'] = 'Telah Ada';
$lang['AMOUNT'] = 'Jumlah';
$lang['AMOUNT_TO_SEND'] = 'Jumlah yang akan dikirim';
$lang['AND'] = 'Dan';
$lang['ANY_SUB_CAT'] = 'Semua Sub-kategori';
$lang['ANYTHING_TO_TELL'] = 'Apakah ada yang ingin Anda sampaikan kepada kami?';
$lang['ANY_CITY'] = 'Kota Manapun';
$lang['ARE_YOU_SURE'] = 'Apakah Anda yakin?';
$lang['AUTHENTICATING'] = 'Sedang mengotentikasi';
$lang['AVATAR'] = 'Avatar';
$lang['AVRAGE_BASED_ON'] = 'rata-rata berdasarkan';
$lang['AVRAGE_RATING'] = 'Penilaian rata-rata';
$lang['BACK'] = 'Kembali';
$lang['BACK_RESULT'] = 'Kembali ke Hasil';
$lang['BANK_ACCOUNT_DETAILS'] = 'Rincian Rekening Bank';
$lang['BANK_CHECK_DETAILS'] = 'Rincian pengiriman Cek Bank';
$lang['BANK_DEPOSIT'] = 'Deposit bank';
$lang['BANK_DEPOST_OFF_PAY'] = 'Deposit Bank (Pembayaran Offline)';
$lang['BROWSE_LISTING'] = 'Jelajahi Daftar Kami';
$lang['BY_CLICK_REGISTER'] = 'Dengan mengklik tombol "Daftar Sekarang", Anda setuju dengan';
$lang['BY_EMAIL'] = 'Dengan Email';
$lang['BY_PHONE'] = 'Dengan No. Telepon';
$lang['CANCEL'] = 'Batal';
$lang['CARD_NUMBER'] = 'NOMOR KARTU';
$lang['CATEGORY_NOTE'] = 'JANGAN memposting banyak iklan untuk item atau layanan yang sama. Semua duplikat, spam, dan iklan yang salah dikategorikan akan dihapus.';
$lang['CAT_REQ'] = 'Kategori dan subkategori wajib diisi.';
$lang['CHANGE_COLOR'] = 'Ubah Warna';
$lang['CHANGE_PASS'] = 'Ubah Kata Sandi';
$lang['CHANGE_THEME'] = 'Ubah Tema';
$lang['CHANGE_COUNTRY'] = 'Ubah Negara';
$lang['CHANGE_PLAN'] = 'Ubah Plan';
$lang['CHANGE_REGION'] = 'Ubah Daerah';
$lang['CHAR_LEFT'] = 'karakter yang tersisa';
$lang['CHECK_BEFORE_PUBLISH'] = 'Periksa item sebelum dipublikasikan!';
$lang['CHECKEMAILFORGOT'] = 'Silakan periksa akun email Anda untuk detail lupa kata sandi!';
$lang['CHECK_BY_TEAM'] = 'Iklan Anda akan tayang setelah diperiksa oleh peninjau.';
$lang['CHOOSE_CATEGORY'] = 'Pilih Kategori';
$lang['CHOOSE_FILE'] = 'Pilih File';
$lang['CITY'] = 'Kota';
$lang['CITY_REQ'] = 'Kota wajib diisi.';
$lang['CLASSIFIED'] = 'Terklasifikasi';
$lang['CLICK_CON'] = 'Dengan mengklik Buat Iklan, Anda menyetujui';
$lang['CLICK_HERE'] = 'klik disini';
$lang['CLICK_HERE_ADD_FILES'] = 'Klik untuk menambahkan file';
$lang['CLOSE_MODAL'] = 'klik untuk menutup modal';
$lang['COLOR_SWITCHER'] = 'Pengalih Warna Gaya';
$lang['COMMENT'] = 'Komentar';
$lang['COMMENT_PLACEHOLDER'] = 'Anda harus memberikan penjelasan singkat tentang setiap perubahan yang Anda buat.';
$lang['COMMENTS_ENABLED'] = 'Komentar diaktifkan di iklan saya';
$lang['COMMENT_REQ'] = 'Pesan yang Diperlukan untuk Peninjau.';
$lang['COMPANY'] = 'Perusahaan';
$lang['CONFIRMATION_MAIL_SENT'] = 'Surat konfirmasi telah dikirim';
$lang['CONFIRM_PAY'] = 'Konfirmasi dan Bayar';
$lang['CONFIRM_PAYMENT'] = 'Konfirmasi Pembayaran';
$lang['CONFIRM_PAYMENT_TEXT'] = 'Harap konfirmasi, jika Anda ingin meningkatkan versi iklan Anda ke Premium.';
$lang['CONFUSED'] = 'ID konfirmasi tidak ada atau sudah digunakan';
$lang['CONPASS'] = 'Konfirmasi Kata Sandi';
$lang['CONTACT'] = 'Kontak';
$lang['CONTACT_ADVERTISER'] = 'Hubungi Pengiklan';
$lang['CONTACT_INFORMATION'] = 'Informasi Kontak';
$lang['CONTACT_SELLER'] = 'hubungi penjual';
$lang['CONTACT_US'] = 'Hubungi kami';
$lang['CONTACTTHANKS'] = 'Terima kasih sudah menghubungi kami.';
$lang['CONTACT_OPTION'] = 'Opsi Kontak';
$lang['CONTACT_OPTION_PARA'] = 'Tentukan bagaimana Anda ingin berhubungan dengan pelanggan.';
$lang['CONTACT_PAGE_TEXT'] = 'Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur eget leo at velit';
$lang['CONTACT_SUBJECT_START'] = 'Email Website: ';
$lang['COST'] = 'Biaya';
$lang['COUNTRIES'] = 'Negara';
$lang['COUNTRY'] = 'Negara';
$lang['CREATE_AN_ACCOUNT'] = 'Buat Akun';
$lang['CREATE_NEW_ACCOUNT'] = 'Buat Akun Baru';
$lang['CREDIT_DEBIT_CARD'] = 'Kartu Kredit / Debit';
$lang['CURRENT_PLAN'] = 'Plan Saat Ini';
$lang['CV_CODE'] = 'KODE CV';
$lang['DAILY'] = 'Harian';
$lang['DASHBOARD'] = 'Dashboard';
$lang['DATE'] = 'Tanggal';
$lang['DATE_CAP'] = 'TANGGAL';
$lang['DAY'] = 'hari';
$lang['DAYS'] = 'hari';
$lang['DECLINED_TRANSACTION'] = 'Transaksi Ditolak';
$lang['DELETE'] = 'Hapus';
$lang['DELETED'] = 'Terhapus';
$lang['DESCRIPTION'] = 'Deskripsi';
$lang['DESC_REQ'] = 'Deskripsi Iklan diperlukan.';
$lang['DISPLAY'] = 'Tampilan';
$lang['DOCUMENTS'] = 'Dokumen';
$lang['DRAG_IMAGE_HERE'] = 'Klik atau tarik gambar ke sini (Maksimal 5)';
$lang['DRAG_MAP_MARKER'] = 'Tarik penanda peta ke alamat yang tepat.';
$lang['DRAG_FILES_HERE'] = 'Tarik file ke sini';
$lang['DURATION'] = 'Durasi';
$lang['EDIT'] = 'Ubah';
$lang['EDIT_AD'] = 'Ubah Iklan';
$lang['EDIT_PROFILE'] = 'Ubah Profil';
$lang['EDIT_CATEGORY'] = 'Ubah Kategori';
$lang['EMAIL'] = 'Email';
$lang['EMAILAD'] = 'Alamat Email';
$lang['EMAILAVL'] = 'Alamat email tersedia';
$lang['EMAILBAN'] = 'Alamat email dilarang';
$lang['EMAILCONFIRM'] = 'Konfirmasi email';
$lang['EMAILINV'] = 'Ini bukan alamat email yang valid';
$lang['EMAILNOTEXIST'] = 'Alamat email tidak ada';
$lang['ENABLE_CHAT_YOURSELF'] = 'Tidak dapat mengobrol dengan diri sendiri.';
$lang['ENQUIRY_FORM'] = 'Formulir Pertanyaan';
$lang['ENTER_MAIL'] = 'Masukan Email';
$lang['ENTER_YOUR_MESSAGE'] = 'Masukkan pesan Anda';
$lang['ENTEREMAIL'] = 'Silahkan masukan alamat email';
$lang['ENTERPASS'] = 'Silakan masukkan kata sandi';
$lang['ENTERUNAME'] = 'Silakan masukkan nama pengguna';
$lang['ENTER_DETAILS'] = 'Silakan masukkan detail Anda di bawah ini';
$lang['ENTER_FULL_NAME'] = 'Masukkan nama lengkap Anda.';
$lang['ENTER_PASS_LINK_AC'] = 'Masukkan kata sandi Anda di bawah ini untuk menautkan akun:';
$lang['ENTER_REVIEW'] = 'Silakan masukkan ulasan Anda.';
$lang['ERROR'] = 'Terjadi Kesalahan';
$lang['ERROR_TRANSACTION'] = 'Terjadi Kesalahan pada Transaksi';
$lang['ERROR_TRY_AGAIN'] = 'Terjadi Kesalahan. Silakan coba lagi.';
$lang['ERROR_UPLOAD_IMG'] = 'Maaf, terjadi kesalahan saat mengupload gambar Iklan.';
$lang['ERROR_WITH_EMAIL'] = 'Terjadi Kesalahan dengan alamat';
$lang['EXAMPLE_RPMV'] = 'Contoh: real estate, property, modern, villa';
$lang['EXP'] = 'EXP';
$lang['EXPIRATION'] = 'EXPIRATION';
$lang['EXPIRY_DATE'] = 'Tanggal Kadaluarsa';
$lang['FAILED_TRANSACTION'] = 'Transaksi Gagal';
$lang['FAKEPROJ'] = 'Iklan palsu diposting';
$lang['FAQ'] = 'FAQ';
$lang['FAQCONTENT_REQ'] = 'Isian konten FAQ diperlukan';
$lang['FAQTITLE_REQ'] = 'Isian judul FAQ diperlukan';
$lang['FAST_EASY_FREE'] = 'Cepat, mudah, dan gratis untuk memasang iklan! Anda akan menemukan apa yang Anda cari.';
$lang['FAVOURITE_ADS'] = 'Iklan Favorit';
$lang['FAVOURITES'] = 'Favorit';
$lang['FEATURED'] = 'Unggulan';
$lang['FEATURED_AD'] = 'Iklan Unggulan';
$lang['FEATURED_ADS'] = 'Iklan Unggulan';
$lang['FEATURED_AD_TEXT'] = 'Iklan unggulan menarik pemirsa berkualitas lebih tinggi dan ditampilkan secara mencolok di beranda bagian Iklan unggulan.';
$lang['FEATURED_FEE'] = 'Biaya iklan unggulan';
$lang['FEEDBACK'] = 'Umpan Balik';
$lang['FEEDBACKTHANKS'] = 'Terima kasih atas tanggapan Anda.';
$lang['FIELD_REQ'] = 'Isian ini diperlukan.';
$lang['FILENAME'] = 'Nama File';
$lang['FIND_ADS'] = 'Cari Iklan';
$lang['FIND_AN_ITEM'] = 'Temukan item';
$lang['FIRST_NAME'] = 'Nama Depan';
$lang['FOLLOW_US'] = 'Ikuti Kami';
$lang['FOLLOWING_SENT_AUTO'] = 'Berikut ini dikirim secara otomatis dari';
$lang['FOOTER_TEXT'] = 'Ngoopin menawarkan iklan baris gratis yang berfokus di Bali. Ngoopin adalah generasi berikutnya dari iklan baris online gratis. Ngoopin memberikan solusi sederhana untuk kerumitan dalam menjual, membeli, berdagang, berdiskusi, mengatur, dan bertemu orang-orang di dekat Anda.';
$lang['FOR'] = 'untuk';
$lang['FORGOTEXP'] = 'Kode Lupa Kata Sandi telah kedaluwarsa';
$lang['FORGOTINV'] = 'Kode Lupa Kata Sandi tidak valid';
$lang['FORGOTPASS'] = 'Lupa kata sandi?';
$lang['FREE'] = 'Gratis';
$lang['FREE_AD'] = 'Iklan Gratis';
$lang['FREE_ADS'] = 'Iklan Gratis';
$lang['FRENCH'] = 'Prancis';
$lang['FROM'] = 'Dari';
$lang['FULL_NAME'] = 'Nama Lengkap';
$lang['GENDER'] = 'Gender';
$lang['GET_START_NOW'] = 'Mulai Sekarang';
$lang['GET_TOUCH'] = 'Hubungi Sekarang';
$lang['GO_HOME'] = 'Kembali ke Home';
$lang['GOTO_UR_EMAIL'] = 'Buka email Anda';
$lang['HELLO'] = 'Halo';
$lang['HELP'] = 'Bantuan';
$lang['HELP_SUPPORT'] = 'Bantuan & Dukungan';
$lang['HIDDEN'] = 'Tersembunyi';
$lang['HIDDEN_ADS'] = 'Iklan Tersembunyi';
$lang['HIDE'] = 'Sembunyikan';
$lang['HIDE_NO'] = 'Sembunyikan nomor telepon di iklan ini';
$lang['HIGH_TO_LOW'] = 'Tinggi ke Rendah';
$lang['HIGHLIGHT'] = 'Sorotan';
$lang['HIGHLIGHT_AD'] = 'Iklan Sorotan';
$lang['HIGHLIGHT_ADS'] = 'Iklan Sorotan';
$lang['HIGHLIGHT_AD_TEXT'] = 'Buat iklan Anda disorot dengan batas di halaman hasil pencarian daftar. Mudah untuk fokus.';
$lang['HIGHLIGHT_FEE'] = 'Biaya iklan sorotan';
$lang['HOME'] = 'Home';
$lang['HOME_IMAGE'] = 'Home dengan gambar';
$lang['HOME_MAP'] = 'Home dengan peta';
$lang['HOME_BANNER_HEADING'] = 'Direktori iklan terlengkap di Bali';
$lang['HOME_BANNER_TAGLINE'] = 'Cari ribuan iklan baris di satu tempat. Mudah, murah, dan cepat.';
$lang['HOUR'] = 'jam';
$lang['HOURS'] = 'jam';
$lang['HOW_IT_WORK'] = 'Bagaimana Cara Kerja';
$lang['HOW_SELL_QUICK'] = 'Cara menjual dengan cepat';
$lang['HOW_WOULD_RATE'] = 'Bagaimana Anda menilai produk ini?';
$lang['I_AGREE_TC'] = 'Saya menyetujui syarat dan ketentuan.';
$lang['ID'] = 'ID';
$lang['IN'] = 'dalam';
$lang['INFORMATION'] = 'Informasi';
$lang['INSTANT_MESSAGE'] = 'Pesan Singkat';
$lang['INVALID_PAYMENT_PROCESS'] = 'Prosesor Pembayaran Tidak Valid';
$lang['INVALID_TRANSACTION'] = 'Transaksi Tidak Valid';
$lang['INV_CVV'] = 'CVV Tidak Valid.';
$lang['INV_EXP_DATE'] = 'Tanggal kedaluwarsa tidak valid.';
$lang['ISINVALID'] = 'tidak valid';
$lang['ITALIAN'] = 'Italia';
$lang['ITEM_DETAILS'] = 'Detail Item';
$lang['JOIN_DATE'] = 'Tanggal Mengikuti ';
$lang['JOINED'] = 'Mengikuti';
$lang['JUST_NOW'] = 'Baru saja';
$lang['KEEP_ME_LOGIN'] = 'Biarkan saya tetap masuk';
$lang['LAST_NAME'] = 'Nama Belakang';
$lang['LAST_SEEN'] = 'Terakhir terlihat';
$lang['LAST_VISITED'] = 'Terakhir dikunjungi';
$lang['LATEST_ADS'] = 'Iklan Terbaru';
$lang['LIMIT_ORDER'] = 'Batas Pesanan';
$lang['LINK_ACCOUNT'] = 'Tautkan Akun';
$lang['LINK_ACCOUNT_SUCCESS'] = 'Akun Berhasil Tertaut! Mengarahkan...';
$lang['LINK_EXIST_ACCOUNT'] = 'Tautkan ke akun yang telah ada';
$lang['LIST_CAT_SUBCAT'] = 'Daftar Kategori dan Subkategori';
$lang['LISTING'] = 'Daftar';
$lang['LISTINGS'] = 'Daftar';
$lang['LOCATED_IN'] = 'Berlokasi di';
$lang['LOCATION'] = 'Lokasi';
$lang['LOC_REQ'] = 'Lokasi iklan diperlukan';
$lang['LOGGED_IN_SUCCESS'] = 'Berhasil masuk! Mengarahkan...';
$lang['LOGIN'] = 'Masuk';
$lang['LOGIN_FACEBOOK'] = 'Masuk dengan Facebook';
$lang['LOGIN_GOOGLE'] = 'Masuk dengan Google+';
$lang['LOGIN_HERE'] = 'Masuk Disini';
$lang['LOGINTOREVIEW'] = 'Masuk untuk menulis ulasan';
$lang['LOGOUT'] = 'Keluar';
$lang['LOW_TO_HIGH'] = 'Rendah ke Tinggi';
$lang['MAILSENTTOSELLER'] = 'Terima kasih telah menggunakan situs kami. Email Anda berhasil dikirim ke penjual.';
$lang['MAIN_IMAGE'] = 'Gambar Utama';
$lang['MAKE_PREMIUM'] = 'Jadikan Iklan Anda Premium';
$lang['MAKE_TRANSACTION'] = 'melakukan transaksi';
$lang['MEMBERSHIP'] = 'Keanggotaan';
$lang['MEMBERSHIPPLAN'] = 'Plan Keanggotaan';
$lang['MESSAGE'] = 'Pesan';
$lang['MINUTE'] = 'menit';
$lang['MINUTES'] = 'menit';
$lang['MOB_NO'] = 'Nomor HP';
$lang['MONTH'] = 'bulan';
$lang['MONTHLY'] = 'Bulanan';
$lang['MONTHS'] = 'bulan';
$lang['MORE_ADS'] = 'Iklan lainnya oleh';
$lang['MORE_INFO'] = 'Info Selengkapnya';
$lang['MORE_RESULTS_FOR'] = 'Hasil selengkapnya untuk';
$lang['MSG_REVIEWER'] = 'Pesan kepada Pengulas';
$lang['MY_ACCOUNT'] = 'Akun Saya';
$lang['MY_ADS'] = 'Iklan Saya';
$lang['MY_ADS_LISTINGS'] = 'Daftar Iklan Saya';
$lang['MY_CLASSIFIED'] = 'Klasifikasi Saya';
$lang['MY_DETAILS'] = 'Detail Saya';
$lang['MY_FAVOURITE_ADS'] = 'Iklan Favorit Saya';
$lang['MY_LISTINGS'] = 'Daftar Saya';
$lang['MY_PENDING_ADS'] = 'Iklan Pending Saya';
$lang['MY_PROFILE'] = 'Profil Saya';
$lang['NAME'] = 'Nama';
$lang['NAMELEN'] = 'Nama harus terdiri dari 4 hingga 20 karakter.';
$lang['NEGOTIABLE_PRICE'] = 'Negotiable Price';
$lang['NEGOTIATE'] = 'negosiasi';
$lang['NEWEST'] = 'Terbaru';
$lang['NEWPASS'] = 'Kata Sandi Baru';
$lang['NEWSLETTER'] = 'Buletin';
$lang['NEXT'] = 'Selanjutnya';
$lang['NO_AD_FAVOURITE'] = 'Tidak ada iklan di daftar favorit.';
$lang['NO_CITY_AVAILABLE'] = 'Tidak ada kota yang tersedia.';
$lang['NO_RESULT_FOUND'] = 'Tidak ada hasil yang ditemukan.';
$lang['NOT_FIND_PAGE'] = 'Kami tidak dapat menemukan halaman yang Anda cari.';
$lang['NOT_FOUND_PAYMENT'] = 'Prosesor Pembayaran Tidak Valid';
$lang['NOTIFYEMAIL'] = 'Beri tahu saya melalui email jika ada iklan yang relevan dengan pilihan saya yang diposting.';
$lang['NO_FOUND'] = 'Tidak ada yang ditemukan.';
$lang['NO_REVIEW'] = 'Belum ada review untuk produk ini.';
$lang['OFFLINE_CREDIT_NOTE'] = 'Sertakan catatan dengan Referensi sehingga kami tahu akun mana yang akan dikreditkan.';
$lang['OFFLINE_PAYMENT_REQUEST'] = 'Kami telah menerima permintaan pembayaran offline Anda. Kami akan menunggu untuk menerima pembayaran Anda untuk memproses permintaan Anda.';
$lang['ONLY_JPG_ALLOW'] = 'Maaf, hanya file JPG yang diperbolehkan.';
$lang['ONLY_LETTER_SPACE'] = 'Hanya huruf dan spasi yang valid [A-Z dan a-z] yang diperbolehkan.';
$lang['OPTION'] = 'Pilihan';
$lang['OPTIONAL'] = 'Opsional';
$lang['ORDER'] = 'Order';
$lang['OTHER'] = 'Lainnya';
$lang['PACKAGE'] = 'Paket';
$lang['PACKAGE_SUMMARY'] = 'Ringkasan Paket';
$lang['PAGE'] = 'Halaman';
$lang['PAGE_NOT_FOUND'] = 'Halaman tidak ditemukan!';
$lang['PAGECONTENT_REQ'] = 'Konten halaman content diperlukan';
$lang['PAGENAME_REQ'] = 'Nama halaman diperlukan';
$lang['PAGENOTEXIST'] = 'Mohon maaf, halaman yang Anda minta tidak dapat ditemukan. Anda dapat kembali ke';
$lang['PAGETITLE_REQ'] = 'Judul halaman diperlukan';
$lang['PASSCHANGED'] = 'Kata sandi Anda telah diubah, silakan masuk untuk melanjutkan.';
$lang['PASSLENG'] = 'Panjang sandi harus antara 4 dan 20 karakter';
$lang['PASSNOMATCH'] = 'Kata sandi yang Anda masukkan tidak cocok';
$lang['PASSWORD'] = 'Kata Sandi';
$lang['PAY_AMOUNT'] = 'Jumlah Pembayaran';
$lang['PAYMENT'] = 'Pembayaran';
$lang['PAYMENTSUCCESS'] = 'Pembayaran Berhasil!';
$lang['PAYMENT_METHOD'] = 'Metode Pembayaran';
$lang['PAYMENT_METHOD_REQ'] = 'Pilih metode pembayaran untuk membuat iklan premium';
$lang['PENDING_ADS'] = 'Iklan Pending';
$lang['PENDING_APPROVAL'] = 'Menunggu persetujuan';
$lang['PHONE'] = 'Telepon';
$lang['PHONE_NO'] = 'Nomor Telepon';
$lang['PHOTO'] = 'Foto';
$lang['PHOTOS'] = 'Foto';
$lang['PIC_REQ'] = 'Gambar iklan diperlukan.';
$lang['POPULAR_CITIES'] = 'Kota populer';
$lang['POST_AD'] = 'Pasang Iklan';
$lang['POST_ADVERTISE'] = 'Pasang Iklan';
$lang['POST_ADVERTISE_QUTO'] = 'Dapatkan penawaran gratis dari orang-orang target dalam hitungan menit, kontak email, peringkat, dan mengobrol dengan mereka.';
$lang['POST_CORRECT'] = 'Pastikan Anda memposting dalam kategori yang benar';
$lang['POST_FREE_AD'] = 'Posting Iklan Gratis';
$lang['POST_FREE_BUSINESS'] = 'Apakah Anda memiliki sesuatu untuk dijual, disewakan, layanan untuk ditawarkan atau tawaran pekerjaan? Posting di Ngoopin sekarang juga, gratis untuk bisnis lokal, dan sangat mudah digunakan.';
$lang['POST_FREE_CLASS'] = 'Pasang Iklan Baris Gratis';
$lang['POST_Y_AD'] = 'Pasang Iklan Anda';
$lang['POSTAD_JUST'] = 'Pasang Iklan hanya dalam';
$lang['POSTCODE'] = 'Kode Pos';
$lang['POSTCONTACT'] = 'Informasi kontak yang posting';
$lang['POSTED_BY'] = 'Diposting oleh';
$lang['POSTED_ON'] = 'Diposting pada';
$lang['POST_SAVE_ERROR'] = 'Iklan Anda tidak dapat disimpan! Coba lagi nanti.';
$lang['PRE_AD_PROMOTE'] = 'Iklan premium membantu penjual mempromosikan produk atau layanan mereka dengan membuat iklan mereka lebih terlihat dengan lebih banyak pembeli dan menjual apa yang mereka inginkan lebih cepat.';
$lang['PRE_SELECTED'] = 'Dipilih Sebelumnya';
$lang['PREFERENCES_SETTING'] = 'Pengaturan Preferensi';
$lang['PREMIUM'] = 'Premium';
$lang['PREMIUM_ADS'] = 'Iklan Premium';
$lang['PREVIEW'] = 'Pratinjau';
$lang['PREVIOUS'] = 'Sebelumnya';
$lang['PRICE'] = 'Harga';
$lang['PRICE_MUST_NO'] = 'Harga iklan harus berupa angka.';
$lang['PRIVACY'] = 'Privasi';
$lang['PROCCESSING'] = 'Memproses';
$lang['PRODUCT_TAG'] = 'Tag Produk';
$lang['PROFILE'] = 'Profil';
$lang['PROFILE_PUBLIC'] = 'Profil tampilan publik';
$lang['PROFILE_TAGLINE'] = 'Tagline Profil';
$lang['PROFILE_UPDATED'] = 'Profil Berhasil Diperbarui';
$lang['RATING'] = 'Peringkat';
$lang['RATING_LOGIN_EROR'] = 'Ulasan Anda tidak dapat disimpan! Silahkan Login untuk menulis review.';
$lang['RATING_SAVED'] = 'Ulasan Anda telah disimpan.';
$lang['RATING_SAVE_ERROR'] = 'Ulasan Anda tidak dapat disimpan! Coba lagi nanti.';
$lang['RE_SUBISSION'] = 'Pengajuan Ulang Iklan';
$lang['RE_SUBISSION_TEXT'] = 'Pengajuan ulang iklan harus ditinjau. Tim kami akan memeriksa konten tersebut sebelum menyetujuinya.';
$lang['READ_MORE'] = 'Baca selengkapnya';
$lang['REASONABLE_PRICE'] = 'Berikan harga yang sewajarnya';
$lang['RECAPTCHA_CLICK'] = 'Silakan klik kotak reCAPTCHA.';
$lang['RECAPTCHA_ERROR'] = 'Verifikasi reCAPTCHA gagal, coba lagi.';
$lang['RECEIVE_NEWSLETTER'] = 'Saya ingin menerima buletin';
$lang['RECIPIENT'] = 'Penerima';
$lang['RECOMMENDED'] = 'DIREKOMENDASIKAN';
$lang['RECOMMENDED_ADS'] = 'Iklan yang Direkomendasikan untuk Anda';
$lang['REDIRECT_PAYPAL'] = 'Anda akan diarahkan ke Paypal untuk menyelesaikan pembayaran.';
$lang['REDIRECT_PAYSTACK'] = 'Anda akan diarahkan ke Paystack untuk menyelesaikan pembayaran.';
$lang['REFERENCE'] = 'Referensi';
$lang['REGION'] = 'Wilayah';
$lang['REGISTER'] = 'Daftar';
$lang['REGISTER_NOW'] = 'Daftar Sekarang';
$lang['REMOVE_FAVOURITE'] = 'Hapus Favorit';
$lang['REPLY_MAIL'] = 'Balas melalui email';
$lang['REPORT'] = 'Laporkan';
$lang['REPORT_THIS_AD'] = 'Laporkan iklan ini';
$lang['REPORTVIO'] = 'Laporkan Pelanggaran';
$lang['REPORT_THANKS'] = 'Terima kasih telah melaporkan pelanggaran ini.';
$lang['REQ_PASS'] = 'Request Kata Sandi';
$lang['RESEND_EMAIL'] = 'Kirim Ulang Email';
$lang['RESUBMITED'] = 'Dikirim Ulang';
$lang['RESUBMITED_ADS'] = 'Iklan yang Dikirim Ulang';
$lang['RESUMIT_EXIST_TEXT'] = 'Iklan Anda telah dikirim dan sedang menunggu persetujuan. Jika Anda ingin melakukan perubahan, Anda dapat menghapusnya dari iklan yang dikirim ulang dan mengirimkannya lagi.';
$lang['REVIEWS'] = 'Ulasan';
$lang['SAVE'] = 'Save';
$lang['SAVE_AS_FAVOURITE'] = 'Simpan iklan sebagai Favorit';
$lang['SAVED_SUCCESS'] = 'Berhasil Disimpan';
$lang['SEARCH'] = 'Cari';
$lang['SEARCH_FILTER'] = 'Filter Pencarian';
$lang['SEARCH_NOW'] = 'Cari Sekarang';
$lang['SEARCH_THOU_CLASSIFY'] = 'Cari ribuan iklan baris di satu tempat';
$lang['SEARCHBYUNOREM'] = 'Cari Berdasarkan Nama Pengguna atau Email';
$lang['SECOND'] = 'detik';
$lang['SECONDS'] = 'detik';
$lang['SELECT'] = 'Pilih';
$lang['SELECT_CATEGORY'] = 'Pilih Kategori';
$lang['SELECT_PAY_METHOD'] = 'Silakan pilih metode pembayaran yang diinginkan';
$lang['SELECT_SUBCATEGORY'] = 'Pilih Subkategori';
$lang['SELECT_CITY'] = 'Pilih Kota';
$lang['SELECT_YOUR_COUNTRY'] = 'Pilh negara Anda';
$lang['SELLER_INFO'] = 'Informasi Penjual';
$lang['SELLER_EMAIL'] = 'Email Penjual';
$lang['SELLER_EMAIL_REQ'] = 'Email penjual diperlukan';
$lang['SELLER_NAME'] = 'Nama Penjual';
$lang['SELLER_NAME_REQ'] = 'Nama penjual diperlukan';
$lang['SEND'] = 'Kirim';
$lang['SEND_FEEDBACK'] = 'Kirimkan umpan balik';
$lang['SEND_MAIL'] = 'Kirim Email';
$lang['SEND_MESSAGE'] = 'Kirim Pesan';
$lang['SENDLINK_NEWPASS'] = 'Masukkan alamat email Anda di bawah ini, kemudian kami akan mengirimkan link untuk membuat kata sandi baru.';
$lang['SENT'] = 'Terkirim';
$lang['SETTING'] = 'Pengaturan';
$lang['SETTING_SAVED'] = 'Pengaturan disimpan';
$lang['SETTING_SAVED_SUCCESS'] = 'Pengaturan Berhasil Disimpan';
$lang['SHARE_AD'] = 'Bagikan iklan ini';
$lang['SHOW'] = 'Tampilkan';
$lang['SHOW_LESS'] = 'Tampilkan lebih sedikit';
$lang['SHOW_MORE'] = 'Tampilkan lebih banyak';
$lang['SHOW_IN_HOME_SEARCH'] = 'Tampilkan iklan di halaman beranda dan daftar hasil pencarian';
$lang['SHOW_ON_HOME'] = 'Tampilkan iklan di halaman beranda dan bagian iklan premium';
$lang['SIGN_IN'] = 'Masuk';
$lang['SIGN_OUT'] = 'Keluar';
$lang['SIGNUP'] = 'Daftar';
$lang['SIMILAR_ADS'] = 'Iklan Serupa';
$lang['SITE_MAP'] = 'Peta-Situs';
$lang['SITEMAP'] = 'Peta Situs';
$lang['SIZE'] = 'Ukuran';
$lang['SOCIAL_NETWORKS'] = 'Jaringan Sosial';
$lang['SORT_BY'] = 'Urutkan berdasarkan';
$lang['SPANISH'] = 'Spanyol';
$lang['SP_PAGE_ADDED'] = 'Halaman Konten Ditambahkan';
$lang['SP_PAGE_EDITED'] = 'Halaman Konten Diedit';
$lang['STAR'] = 'Bintang';
$lang['START_DATE'] = 'Tanggal Mulai';
$lang['STATE'] = 'NEGARA BAGIAN';
$lang['STATE_REQ'] = 'Negara bagian diperlukan.';
$lang['STATUS'] = 'Status';
$lang['STOP_UPLOAD'] = 'Berhenti Mengunggah';
$lang['SUB_CATEGORY'] = 'Sub Kategori';
$lang['SUBJECT'] = 'Subyek';
$lang['SUBMIT'] = 'Kirim';
$lang['SUBMITREVIEWS'] = 'Kirimkan ulasan';
$lang['SUCCESS'] = 'Sukses';
$lang['SUPPORT'] = 'Dukungan';
$lang['TAGS'] = 'Tag';
$lang['TAGS_DETAIL'] = 'Masukkan tag, pisahkan dengan koma.';
$lang['TAG_REQ'] = 'Tag diperlukan.';
$lang['TERM'] = 'PERSYARATAN';
$lang['TERM_CON'] = 'Syarat & Ketentuan';
$lang['THANKCONFIRM'] = 'Terima kasih telah mendaftar! Silakan periksa email Anda untuk kode konfirmasi Anda.';
$lang['THANKS'] = 'Terima kasih';
$lang['THANKSIGNUP'] = 'Terima kasih telah mendaftar!';
$lang['THANKS_YOU'] = 'Terima kasih';
$lang['TITLE'] = 'Judul';
$lang['TO'] = 'Ke';
$lang['TOP_SEARCH_RESULT'] = 'Teratas dalam hasil pencarian dan kategori';
$lang['TORESET'] = 'Untuk mengatur ulang kata sandi Anda, silakan klik tautan di bawah ini.';
$lang['TOTAL'] = 'Total';
$lang['TOTAL_ADS'] = 'Total IKLAN';
$lang['TOTAL_COST'] = 'Total Biaya';
$lang['TOTAL_RECORD'] = 'Total Pencatatan';
$lang['TRANSACTION'] = 'Transaksi';
$lang['TRANS_FAIL'] = 'Status transaksi gagal';
$lang['TRANS_SUCCESS'] = 'Status transaksi sukses';
$lang['TRY_AGAIN'] = 'Coba lagi';
$lang['TYPE'] = 'Tipe';
$lang['TYPE_SOMETHING'] = 'Ketik sesuatu tentang item Anda';
$lang['TYPE_A_MESSAGE'] = 'Ketik pesan';
$lang['TYPE_YOUR_CITY'] = 'Ketik nama kota Anda';
$lang['UNKNOWN_ERROR'] = 'Kesalahan yang tidak diketahui';
$lang['UPDATE'] = 'Update';
$lang['UPDATE_ACCOUNT'] = 'Update Akun';
$lang['UPGRADE'] = 'Upgrade';
$lang['UPGRADES'] = 'Upgrade';
$lang['UPGRADE_TEXT_INFO'] = 'Anda secara opsional dapat memilih beberapa Upgrade untuk mendapatkan hasil terbaik.';
$lang['UPLOAD_PHOTO'] = 'Unggah foto untuk iklan';
$lang['UPLOAD_PICTURE'] = 'Unggah gambar';
$lang['UPLOAD_IMAGES'] = 'Unggah gambar';
$lang['URGENT'] = 'Mendesak';
$lang['URGENT_AD'] = 'Iklan Mendesak';
$lang['URGENT_ADS'] = 'Iklan Mendesak';
$lang['URGENT_AD_TEXT'] = 'Buat iklan Anda menonjol dan beri tahu pemirsa bahwa iklan Anda benar-benar mendesak.';
$lang['URGENT_FEE'] = 'Biaya Iklan Mendesak';
$lang['URLVIOLATION'] = 'Alamat URL pelanggaran';
$lang['USE_BRIEF'] = 'Gunakan judul dan deskripsi item yang singkat';
$lang['USE_REAL_IMAGE'] = 'Gunakan gambar nyata produk/jasa Anda, bukan katalog';
$lang['USER_DETAILS'] = 'Detail Pengguna';
$lang['USER_LOGIN'] = 'Login Pengguna';
$lang['USERALPHA'] = 'Nama pengguna hanya boleh berisi karakter alfanumerik';
$lang['USERBAN'] = 'Nama pengguna ini telah dilarang';
$lang['USERLEN'] = 'Panjang nama pengguna harus antara 4 dan 15 karakter';
$lang['USERNAME'] = 'Nama pengguna';
$lang['USERNOTFOUND'] = 'Nama pengguna atau Password tidak ditemukan';
$lang['USEROTHER'] = 'Nama pengguna orang lain';
$lang['USERUAV'] = 'Nama pengguna tersedia';
$lang['USERUNAV'] = 'Nama pengguna tidak tersedia';
$lang['VAILD_CARD_NUMBER'] = 'Nomor kartu valid';
$lang['VALIDATING'] = 'Memvalidasi';
$lang['VERIFY_EMAIL_ADDRESS'] = 'Verifikasi alamat email Anda.';
$lang['VIDEOS'] = 'Video';
$lang['VIEW_AD'] = 'Lihat Iklan';
$lang['VIEW_MORE'] = 'Lihat Lebih Banyak';
$lang['VIEW_PROFILE'] = 'Lihat profil';
$lang['VIODETAILS'] = 'Detail Pelanggaran';
$lang['VIOLAT0R'] = 'Pelanggar';
$lang['VIOLATION'] = 'Pelanggaran';
$lang['VISITS'] = 'Kunjungan';
$lang['WANT_TO_CONTACT'] = 'ingin menghubungi Anda';
$lang['WEEKLY'] = 'Mingguan';
$lang['WELCOME'] = 'Selamat Datang!';
$lang['WELCOMETOSITE'] = 'Selamat datang di Ngoopin. Dapatkan pengalaman untuk memposting iklan baris gratis. Terima kasih.';
$lang['WELCOME_BACK'] = 'Selamat datang kembali!';
$lang['WHAT'] = 'Apa';
$lang['WHAT_YOU_THINK'] = 'Beri tahu kami pendapat Anda tentang kami!';
$lang['WHAT_LOOK_FOR'] = 'Apa yang ingin Anda cari?';
$lang['WHERE'] = 'Dimana?';
$lang['WHOLE'] = 'Seluruh';
$lang['WITHIN_SECOND'] = 'dalam 10 detik';
$lang['YEAR'] = 'tahun';
$lang['YEARLY'] = 'Tahunan';
$lang['YEARS'] = 'tahun';
$lang['YEMAIL'] = 'E-Mail Anda';
$lang['YES_DELETE'] = 'Ya, hapus';
$lang['YNAME'] = 'Nama Anda';
$lang['YOU_ARE_FORWARD'] = 'jika Anda tidak diteruskan ke';
$lang['YOU_LOGIN'] = 'Anda terakhir masuk pada';
$lang['YOUR_AD'] = 'Iklan Anda';
$lang['YOURREVIEWS'] = 'Ulasan Anda';
$lang['YOUR_CITY'] = 'Kota Anda';
$lang['YOU_WANT_DELETE'] = 'Anda ingin menghapus Iklan ini';
$lang['YUSERNAME'] = 'Nama pengguna Anda';
$lang['ZIPCODE'] = 'Kode Pos';
$lang['EXPIRE_ADS'] = 'Iklan yang Expire';
$lang['RENEW'] = 'Perbarui';
$lang['LOGGEDIN_SUCCESS'] = 'Berhasil masuk!';
$lang['MEMEBERSHIP_CANCEL'] = 'Keanggotaan Anda berhasil dibatalkan.';
$lang['ENGLISH'] = 'Inggris';
$lang['ARABIC'] = 'Arab';
$lang['BULGARIAN'] = 'Bulgaria';
$lang['CHINESE'] = 'Tiongkok';
$lang['FRENCH'] = 'Prancis';
$lang['GERMAN'] = 'Jerman';
$lang['HEBREW'] = 'Ibrani';
$lang['HINDI'] = 'Hindi';
$lang['ITALIAN'] = 'Italia';
$lang['JAPANESE'] = 'Jepang';
$lang['POLISH'] = 'Polandia';
$lang['ROMANIA'] = 'Romania';
$lang['RUSSIAN'] = 'Rusia';
$lang['SPANISH'] = 'Spanyol';
$lang['SWEDISH'] = 'Swedia';
$lang['THAI'] = 'Thailand';
$lang['TURKISH'] = 'Turki';
$lang['URDU'] = 'Urdu';
$lang['VIETNAMESE'] = 'Vietnam';
$lang['WEBSITE'] = 'Website';
$lang['SHARE'] = 'Bagikan';
$lang['LEAVEEMAIL'] = 'Silakan tinggalkan alamat email';
$lang['AD_POST_LIMIT'] = 'Batas Posting Iklan';
$lang['NOTIFY'] = 'Beritahu';
$lang['POST_LIMIT_EXCEED'] = 'Maaf, batas posting iklan Anda sudah terlampaui! Anda harus meningkatkan paket keanggotaan Anda untuk mengirim lebih banyak iklan.';
$lang['PREMIUM_TEXT'] = 'Lihat daftar terbaru yang tersedia di katalog direktori yang ditambahkan oleh pengguna kami. Semua daftar diverifikasi oleh staf editor kami';
$lang['LATEST_TEXT'] = 'Lihat daftar terbaru yang tersedia di katalog direktori yang ditambahkan oleh pengguna kami. Semua daftar diverifikasi oleh staf editor kami';
$lang['RECENT_REVIEWS'] = 'Review Terbaru';
$lang['ADDTO_COMPARE'] = 'Tambahkan untuk membandingkan';
$lang['ADDTO_FAVORITES'] = 'Tambahkan ke Favorit';
$lang['REPORT_LISTING'] = 'Daftar Pelaporan';
$lang['SAFETY_TIPS_FOR_BUYERS'] = 'Tips Keamanan Untuk Pembeli';
$lang['MEET_SELLER_AT_PUBLIC_PLACE'] = 'Temui Penjual di Tempat Umum';
$lang['CHECK_ITEM_BEFORE_YOU_BUY'] = 'Periksa barang sebelum Anda membeli';
$lang['PAY_ONLY_AFTER_COLLECTING_ITEM'] = 'Hanya membayar setelah menerima barang';
$lang['OR'] = 'Atau';
$lang['DONT_HAVE_ACCOUNT'] = 'Belum memiliki akun?';
$lang['SIGNUP_NOW'] = 'Daftar sekarang!';
$lang['LOGIN_VIA_FACEBOOK'] = 'Log In via Facebook';
$lang['LOGIN_VIA_GOOGLE'] = 'Log In via Google';
$lang['LETS_CREATE_ACC'] = 'Ayo buat akun Anda!';
$lang['ALREADY_HAVE_ACC'] = 'Sudah memiliki akun?';
$lang['WE_FOUND'] = 'Kami menemukan';
$lang['ADS_FOUND'] = 'iklan ditemukan';
$lang['SEARCH_RESULTS'] = 'Hasil Pencarian';
$lang['EXPIRING'] = 'Expiring';
$lang['EXPIRED_ON'] = 'Expired pada';
$lang['ADS'] = 'Iklan';
$lang['DASH_NAVIGATION'] = 'Navigasi Dashboard';
$lang['ALL_PLANS'] = 'Semua Plan';
$lang['FEATURES_OF'] = 'Fitur dari';
$lang['CATEGORY'] = 'Kategori';
$lang['ACTIONS'] = 'Aksi';
$lang['SAVE_CHANGES'] = 'Simpan Perubahan';
$lang['LOGO_HINT'] = 'Gunakan ukuran 200x200px untuk tampilan yang lebih baik.';
$lang['TIPS'] = 'Tips!';
$lang['POST_JOB_TIPS1'] = 'Masukkan deskripsi singkat tentang iklan Anda.';
$lang['POST_JOB_TIPS2'] = 'Tambahkan foto iklan Anda.';
$lang['POST_JOB_TIPS3'] = 'Pilih kategori dan subkategori iklan yang benar.';
$lang['POST_JOB_TIPS4'] = 'Harap periksa kembali sebelum mengirimkan iklan.';
$lang['SAVE_THIS_AD'] = 'Simpan Iklan Ini';
$lang['AD_SAVED'] = 'Iklan Tersimpan!';
$lang['BOOKMARK_SHARE'] = 'Simpan atau Bagikan';
$lang['SHOW_PHONE_NO'] = 'Tampilkan No. Telepon';
$lang['SHARE_EMAIL'] = 'Bagikan ke Email';
$lang['SHARE_FACEBOOK'] = 'Bagikan ke Facebook';
$lang['SHARE_TWITTER'] = 'Bagikan ke Twitter';
$lang['SHARE_LINKEDIN'] = 'Bagikan ke LinkedIn';
$lang['SHARE_PINTEREST'] = 'Bagikan ke Pinterest';
$lang['SHARE_WHATSAPP'] = 'Bagikan ke WhatsApp';
$lang['REDIRECT_PAYTM'] = 'Anda akan diarahkan ke Paytm untuk menyelesaikan pembayaran.';
$lang['REDIRECT_CCAVENUE'] = 'Anda akan diarahkan ke CCAvenue untuk menyelesaikan pembayaran.';
$lang['BLOG'] = 'Blog';
$lang['TESTIMONIALS'] = 'Testimoni';
$lang['REPLY'] = 'Balas';
$lang['CANCEL_REPLY'] = 'Batal membalas';
$lang['BLOG_NOT_FOUND'] = 'Maaf, kami tidak dapat menemukan blog yang Anda cari!';
$lang['AUTHOR'] = 'Penulis';
$lang['COMMENTS'] = 'Komentar';
$lang['POST_COMMENT'] = 'Posting Sebuah Komentar';
$lang['COMMENTING_AS'] = 'Anda memberikan komentar sebagai:';
$lang['YOUR_COMMENT'] = 'Komentar Anda...';
$lang['ALL_FIELDS_REQ'] = 'Semua field yang diperlukan.';
$lang['DUPLICATE_COMMENT'] = 'Komentar Duplikat: Komentar ini sudah ada.';
$lang['COMMENT_REVIEW'] = 'Komentar berhasil diposting, harap tunggu pengulas menyetujuinya.';
$lang['SEARCH_RESULT_FOR'] = 'Hasil pencarian untuk:';
$lang['RECENT_BLOG'] = 'Blog Terbaru';
$lang['BY'] = 'oleh';
$lang['CATEGORIES'] = 'Kategori';
$lang['ADMIN'] = 'Admin';
$lang['COOKIES'] = 'Cookies';
$lang['COOKIES_MESSAGE'] = 'Situs web ini menggunakan cookie untuk memastikan Anda mendapatkan pengalaman terbaik di situs web kami.';
$lang['COOKIES_ACCEPT'] = 'Setuju';
$lang['COOKIE_POLICY'] = 'Kebijakan Cookie';
$lang['CHATS'] = 'Chat';
$lang['NO_MSG_FOUND'] = 'Tidak ada pesan yang ditemukan.';
$lang['ACTIVE'] = 'Active';
$lang['PENDING'] = 'Pending';
$lang['EXPIRE'] = 'Expired';
$lang['FAILED'] = 'Gagal';
$lang['ENTERVIOL'] = 'Harap masukkan detail pelanggaran.';
$lang['LOGIN_CHAT'] = 'Masuk untuk Chat';
$lang['CHAT_NOW'] = 'Chat Sekarang!';
$lang['ABOUT'] = 'Tentang';
$lang['DETAILS'] = 'Detail';
$lang['INQUIRE_FORM'] = 'Formulir Pertanyaan';
$lang['REDIRECT_PAYUMONEY'] = 'Anda akan diarahkan ke PayUMoney untuk menyelesaikan pembayaran.';

$lang['EMAIL_VERIFY_MSG'] = 'Alamat email Anda belum diverifikasi. Harap verifikasi alamat email Anda terlebih dahulu.';
$lang['ONLINE'] = 'Online';
$lang['OFFLINE'] = 'Offline';
$lang['TYPING'] = 'Sedang mengetik...';
$lang['GOT_MESSAGE'] = 'Anda mendapat pesan!';
$lang['LOGIN_POST_COMMENT'] = 'Silakan masuk terlebih dahulu untuk memberi komentar.';
$lang['FIELD_REQUIRED'] = 'Field ini diperlukan.';
$lang['TYPE_ENTER'] = 'ketik dan tekan Enter';

